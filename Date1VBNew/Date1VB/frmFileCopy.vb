Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports System.Reflection
Imports System.Web.UI.WebControls
Imports Spire.Doc

Public Class frmFileCopy
    Inherits System.Windows.Forms.Form

    Friend WithEvents DriveListBox1 As Windows.Forms.ListBox
    Friend WithEvents Drive2ListBox2 As Windows.Forms.ListBox
    Friend WithEvents DirListBox1 As Windows.Forms.ListBox
    Friend WithEvents Dir2ListBox2 As Windows.Forms.ListBox
    Friend WithEvents FileListBox1 As Windows.Forms.ListBox
    Friend WithEvents File2ListBox3 As Windows.Forms.ListBox

#Region " Windows Form Designer generated code "
    Private itemIndex As Integer
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    'Form overrides dispose to clean up the component list.

#End Region
    Private Sub frmFileCopy_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim path As String
        path = ConfigurationSettings.AppSettings("DPath")
        Dim path1 As String
        path1 = ConfigurationSettings.AppSettings("SPath")
        For Each drive As IO.DriveInfo In IO.DriveInfo.GetDrives
            DriveListBox1.Items.Add(drive.Name)
        Next
        For Each drive As IO.DriveInfo In IO.DriveInfo.GetDrives
            Drive2ListBox2.Items.Add(drive.Name)
        Next

        DirListBox1.Items.Add(path1)

        For Each file As String In My.Computer.FileSystem.GetFiles(path1)
            FileListBox1.Items.Add(System.IO.Path.GetFileName(file))
        Next
        For Each drive As IO.DriveInfo In IO.DriveInfo.GetDrives
            Drive2ListBox2.Items.Add(drive.Name)
        Next
        For Each drive As IO.DriveInfo In IO.DriveInfo.GetDrives
            Drive2ListBox2.Items.Add(drive.Name)
        Next

        Dir2ListBox2.Items.Add(path)


        For Each file As String In My.Computer.FileSystem.GetFiles(path)
            File2ListBox3.Items.Add(System.IO.Path.GetFileName(file))
        Next
        File2ListBox3.AllowDrop = True
        FileListBox1.AllowDrop = True

    End Sub
    Private Sub FileListBox1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles FileListBox1.MouseDown

        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim index As Integer = FileListBox1.IndexFromPoint(e.X, e.Y)

            Dim item As String = FileListBox1.Items(index)
            Dim drop_effect As DragDropEffects =
                    FileListBox1.DoDragDrop(
                        FileListBox1.Items(index),
                        DragDropEffects.Move Or DragDropEffects.Copy)

            ' If it was moved, remove the item from this list.
            If drop_effect = DragDropEffects.Move Then
                ' See if the user dropped the item in this ListBox
                ' at a higher position.
                Dim somecondition As Boolean = True
                Dim folder As String
                For i As Integer = FileListBox1.SelectedItems.Count - 1 To 0 Step -1
                    If somecondition = True Then
                        folder = FileListBox1.SelectedItems.Item(i)

                        FileListBox1.Items.Remove(FileListBox1.SelectedItems(i))
                    End If
                Next
            End If
        End If

    End Sub

    ' Display the appropriate cursor.
    Private Sub File2ListBox3_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles File2ListBox3.DragOver
        Const KEY_CTRL As Integer = 8

        If Not (e.Data.GetDataPresent(GetType(System.String))) Then
            e.Effect = DragDropEffects.None
        ElseIf (e.KeyState And KEY_CTRL) And
        (e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy Then
            ' Copy.
            e.Effect = DragDropEffects.Copy
        ElseIf (e.AllowedEffect And DragDropEffects.Move) = DragDropEffects.Move Then
            ' Move.
            e.Effect = DragDropEffects.Move
        End If
    End Sub

    ' Drop the entry in the list.
    Private Sub File2ListBox3_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles File2ListBox3.DragDrop
        Dim Item As String
        Dim path As String
        path = ConfigurationSettings.AppSettings("DPath")
        Dim path1 As String
        path1 = ConfigurationSettings.AppSettings("SPath")
        For Each Item In FileListBox1.SelectedItems


            If (e.Effect = DragDropEffects.Copy) Or
                (e.Effect = DragDropEffects.Move) Then

                Dim itm As Object = Item
                Dim path2 As String = DriveListBox1.SelectedItem
                Dim path3 As String = File2ListBox3.SelectedItem
                File2ListBox3.Items.Add(itm)
                If path2 IsNot Nothing And path3 IsNot Nothing Then
                    My.Computer.FileSystem.CopyFile("" + path2 + "\" + itm + "", "" + path3 + "\" + itm + "",
                Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
                    My.Computer.FileSystem.DeleteFile("" + path2 + "\" + itm + "")
                Else
                    My.Computer.FileSystem.CopyFile("" + path1 + "\" + itm + "", "" + path + "\" + itm + "",
               Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
               Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
                    My.Computer.FileSystem.DeleteFile("" + path1 + "\" + itm + "")
                End If
            Else

            End If


        Next
    End Sub

    Private Sub File2ListBox3_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles File2ListBox3.MouseDown

        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim index As Integer = File2ListBox3.IndexFromPoint(e.X, e.Y)

            Dim item As String = File2ListBox3.Items(index)
            Dim drop_effect As DragDropEffects =
                    File2ListBox3.DoDragDrop(
                        File2ListBox3.Items(index),
                        DragDropEffects.Move Or DragDropEffects.Copy)

            ' If it was moved, remove the item from this list.
            If drop_effect = DragDropEffects.Move Then
                ' See if the user dropped the item in this ListBox
                ' at a higher position.
                Dim somecondition As Boolean = True
                Dim folder As String
                For i As Integer = File2ListBox3.SelectedItems.Count - 1 To 0 Step -1
                    If somecondition = True Then
                        folder = File2ListBox3.SelectedItems.Item(i)

                        File2ListBox3.Items.Remove(File2ListBox3.SelectedItems(i))
                    End If
                Next
            End If
        End If

    End Sub

    ' Display the appropriate cursor.
    Private Sub FileListBox1_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles FileListBox1.DragOver
        Const KEY_CTRL As Integer = 8

        If Not (e.Data.GetDataPresent(GetType(System.String))) Then
            e.Effect = DragDropEffects.None
        ElseIf (e.KeyState And KEY_CTRL) And
        (e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy Then
            ' Copy.
            e.Effect = DragDropEffects.Copy
        ElseIf (e.AllowedEffect And DragDropEffects.Move) = DragDropEffects.Move Then
            ' Move.
            e.Effect = DragDropEffects.Move
        End If
    End Sub

    ' Drop the entry in the list.
    Private Sub FileListBox1_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles FileListBox1.DragDrop
        Dim Item As String
        Dim path As String
        path = ConfigurationSettings.AppSettings("DPath")
        Dim path1 As String
        path1 = ConfigurationSettings.AppSettings("SPath")
        For Each Item In File2ListBox3.SelectedItems


            If (e.Effect = DragDropEffects.Copy) Or
                (e.Effect = DragDropEffects.Move) Then

                Dim itm As Object = Item
                Dim path2 As String = Drive2ListBox2.SelectedItem
                Dim path3 As String = FileListBox1.SelectedItem
                FileListBox1.Items.Add(itm)
                If path2 IsNot Nothing And path3 IsNot Nothing Then
                    My.Computer.FileSystem.CopyFile("" + path2 + "\" + itm + "", "" + path3 + "\" + itm + "",
                Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
                    My.Computer.FileSystem.DeleteFile("" + path2 + "\" + itm + "")
                Else
                    My.Computer.FileSystem.CopyFile("" + path + "\" + itm + "", "" + path1 + "\" + itm + "",
               Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
               Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
                    My.Computer.FileSystem.DeleteFile("" + path + "\" + itm + "")
                End If
            Else

            End If


        Next
    End Sub

    Private Sub InitializeComponent()
        Me.DriveListBox1 = New System.Windows.Forms.ListBox()
        Me.Drive2ListBox2 = New System.Windows.Forms.ListBox()
        Me.DirListBox1 = New System.Windows.Forms.ListBox()
        Me.Dir2ListBox2 = New System.Windows.Forms.ListBox()
        Me.FileListBox1 = New System.Windows.Forms.ListBox()
        Me.File2ListBox3 = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'DriveListBox1
        '
        Me.DriveListBox1.FormattingEnabled = True
        Me.DriveListBox1.ItemHeight = 20
        Me.DriveListBox1.Location = New System.Drawing.Point(26, 30)
        Me.DriveListBox1.Name = "DriveListBox1"
        Me.DriveListBox1.Size = New System.Drawing.Size(303, 44)
        Me.DriveListBox1.TabIndex = 0
        '
        'Drive2ListBox2
        '
        Me.Drive2ListBox2.FormattingEnabled = True
        Me.Drive2ListBox2.ItemHeight = 20
        Me.Drive2ListBox2.Location = New System.Drawing.Point(358, 30)
        Me.Drive2ListBox2.Name = "Drive2ListBox2"
        Me.Drive2ListBox2.Size = New System.Drawing.Size(334, 44)
        Me.Drive2ListBox2.TabIndex = 1
        '
        'DirListBox1
        '
        Me.DirListBox1.FormattingEnabled = True
        Me.DirListBox1.ItemHeight = 20
        Me.DirListBox1.Location = New System.Drawing.Point(26, 104)
        Me.DirListBox1.Name = "DirListBox1"
        Me.DirListBox1.Size = New System.Drawing.Size(303, 184)
        Me.DirListBox1.TabIndex = 2
        '
        'Dir2ListBox2
        '
        Me.Dir2ListBox2.FormattingEnabled = True
        Me.Dir2ListBox2.ItemHeight = 20
        Me.Dir2ListBox2.Location = New System.Drawing.Point(358, 104)
        Me.Dir2ListBox2.Name = "Dir2ListBox2"
        Me.Dir2ListBox2.Size = New System.Drawing.Size(334, 184)
        Me.Dir2ListBox2.TabIndex = 3
        '
        'FileListBox1
        '
        Me.FileListBox1.AllowDrop = True
        Me.FileListBox1.FormattingEnabled = True
        Me.FileListBox1.ItemHeight = 20
        Me.FileListBox1.Location = New System.Drawing.Point(26, 315)
        Me.FileListBox1.Name = "FileListBox1"
        Me.FileListBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.FileListBox1.Size = New System.Drawing.Size(303, 184)
        Me.FileListBox1.TabIndex = 4
        '
        'File2ListBox3
        '
        Me.File2ListBox3.AllowDrop = True
        Me.File2ListBox3.FormattingEnabled = True
        Me.File2ListBox3.ItemHeight = 20
        Me.File2ListBox3.Location = New System.Drawing.Point(358, 315)
        Me.File2ListBox3.Name = "File2ListBox3"
        Me.File2ListBox3.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.File2ListBox3.Size = New System.Drawing.Size(334, 184)
        Me.File2ListBox3.TabIndex = 5
        '
        'frmFileCopy
        '
        Me.ClientSize = New System.Drawing.Size(742, 517)
        Me.Controls.Add(Me.File2ListBox3)
        Me.Controls.Add(Me.FileListBox1)
        Me.Controls.Add(Me.Dir2ListBox2)
        Me.Controls.Add(Me.DirListBox1)
        Me.Controls.Add(Me.Drive2ListBox2)
        Me.Controls.Add(Me.DriveListBox1)
        Me.Name = "frmFileCopy"
        Me.ResumeLayout(False)

    End Sub

    Private Sub DriveListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DriveListBox1.SelectedIndexChanged
        Dim path2 As String = DriveListBox1.SelectedItem
        For Each file As String In My.Computer.FileSystem.GetDirectories(path2)
            DirListBox1.Items.Add(file)
        Next

    End Sub

    Private Sub Drive2ListBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Drive2ListBox2.SelectedIndexChanged
        Dim path2 As String = Drive2ListBox2.SelectedItem
        For Each file As String In My.Computer.FileSystem.GetDirectories(path2)
            Dir2ListBox2.Items.Add(file)
        Next
    End Sub

    Private Sub DirListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DirListBox1.SelectedIndexChanged
        Dim path2 As String = DirListBox1.SelectedItem
        For Each file As String In My.Computer.FileSystem.GetFiles(path2)
            FileListBox1.Items.Add(file)
        Next
    End Sub

    Private Sub Dir2ListBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Dir2ListBox2.SelectedIndexChanged

        Dim path2 As String = Dir2ListBox2.SelectedItem
        For Each file As String In My.Computer.FileSystem.GetFiles(path2)
            File2ListBox3.Items.Add(System.IO.Path.GetFileName(file))
        Next
    End Sub

    'Private Sub FileListBox1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles FileListBox1.MouseDoubleClick
    '    ComponentInfo.SetLicense("FREE-LIMITED-KEY")

    '    ' Create a new empty Word file.
    '    Dim doc = New DocumentModel()

    '    ' Add a new document content.
    '    doc.Sections.Add(New Section(doc, New Paragraph(doc, "Hello world!")))

    '    ' Save to DOCX and PDF files.
    '    doc.Save("Document.docx")
    '    doc.Save("Document.pdf")
    'End Sub
End Class
