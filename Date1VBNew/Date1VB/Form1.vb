Imports System.Collections.Generic
Imports System.Web.UI.WebControls

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents DriveListBox1 As Microsoft.VisualBasic.Compatibility.VB6.DriveListBox
    Friend WithEvents DirListBox1 As Microsoft.VisualBasic.Compatibility.VB6.DirListBox
    Friend WithEvents FileListBox1 As Microsoft.VisualBasic.Compatibility.VB6.FileListBox
    Friend WithEvents lbDir As System.Windows.Forms.Label
    Friend WithEvents Drive2ListBox1 As Microsoft.VisualBasic.Compatibility.VB6.DriveListBox
    Friend WithEvents Dir2ListBox2 As Microsoft.VisualBasic.Compatibility.VB6.DirListBox
    Friend WithEvents File2ListBox3 As Microsoft.VisualBasic.Compatibility.VB6.FileListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.DriveListBox1 = New Microsoft.VisualBasic.Compatibility.VB6.DriveListBox()
        Me.DirListBox1 = New Microsoft.VisualBasic.Compatibility.VB6.DirListBox()
        Me.FileListBox1 = New Microsoft.VisualBasic.Compatibility.VB6.FileListBox()
        Me.lbDir = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Drive2ListBox1 = New Microsoft.VisualBasic.Compatibility.VB6.DriveListBox()
        Me.Dir2ListBox2 = New Microsoft.VisualBasic.Compatibility.VB6.DirListBox()
        Me.File2ListBox3 = New Microsoft.VisualBasic.Compatibility.VB6.FileListBox()
        Me.SuspendLayout()
        '
        'DriveListBox1
        '
        Me.DriveListBox1.FormattingEnabled = True
        Me.DriveListBox1.Location = New System.Drawing.Point(89, 20)
        Me.DriveListBox1.Name = "DriveListBox1"
        Me.DriveListBox1.Size = New System.Drawing.Size(152, 21)
        Me.DriveListBox1.TabIndex = 4
        '
        'DirListBox1
        '
        Me.DirListBox1.FormattingEnabled = True
        Me.DirListBox1.IntegralHeight = False
        Me.DirListBox1.Location = New System.Drawing.Point(89, 57)
        Me.DirListBox1.Name = "DirListBox1"
        Me.DirListBox1.Size = New System.Drawing.Size(168, 160)
        Me.DirListBox1.TabIndex = 5
        '
        'FileListBox1
        '
        Me.FileListBox1.AllowDrop = True
        Me.FileListBox1.FormattingEnabled = True
        Me.FileListBox1.Location = New System.Drawing.Point(89, 246)
        Me.FileListBox1.Name = "FileListBox1"
        Me.FileListBox1.Pattern = "*.*"
        Me.FileListBox1.Size = New System.Drawing.Size(184, 160)
        Me.FileListBox1.TabIndex = 6
        '
        'lbDir
        '
        Me.lbDir.Location = New System.Drawing.Point(247, 20)
        Me.lbDir.Name = "lbDir"
        Me.lbDir.Size = New System.Drawing.Size(168, 23)
        Me.lbDir.TabIndex = 7
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(616, 17)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 24)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "'Q'"
        '
        'Drive2ListBox1
        '
        Me.Drive2ListBox1.FormattingEnabled = True
        Me.Drive2ListBox1.Location = New System.Drawing.Point(392, 23)
        Me.Drive2ListBox1.Name = "Drive2ListBox1"
        Me.Drive2ListBox1.Size = New System.Drawing.Size(164, 21)
        Me.Drive2ListBox1.TabIndex = 9
        '
        'Dir2ListBox2
        '
        Me.Dir2ListBox2.FormattingEnabled = True
        Me.Dir2ListBox2.IntegralHeight = False
        Me.Dir2ListBox2.Location = New System.Drawing.Point(392, 57)
        Me.Dir2ListBox2.Name = "Dir2ListBox2"
        Me.Dir2ListBox2.Size = New System.Drawing.Size(164, 160)
        Me.Dir2ListBox2.TabIndex = 10
        '
        'File2ListBox3
        '
        Me.File2ListBox3.AllowDrop = True
        Me.File2ListBox3.FormattingEnabled = True
        Me.File2ListBox3.Location = New System.Drawing.Point(392, 233)
        Me.File2ListBox3.Name = "File2ListBox3"
        Me.File2ListBox3.Pattern = "*.*"
        Me.File2ListBox3.Size = New System.Drawing.Size(179, 173)
        Me.File2ListBox3.TabIndex = 11
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1233, 487)
        Me.Controls.Add(Me.File2ListBox3)
        Me.Controls.Add(Me.Dir2ListBox2)
        Me.Controls.Add(Me.Drive2ListBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lbDir)
        Me.Controls.Add(Me.FileListBox1)
        Me.Controls.Add(Me.DirListBox1)
        Me.Controls.Add(Me.DriveListBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    txtop.Text = Convert.ToDateTime(txtip.Text).ToString("MM/d/yy")
    'End Sub

    'Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    txtop.Text = Convert.ToDateTime(txtip.Text).ToString("d/MM/yy")
    'End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dispose()
    End Sub

    Private Sub DriveListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DriveListBox1.SelectedIndexChanged
        DirListBox1.Path = DriveListBox1.Drive ' When drive changes, set directory path.
    End Sub

    Private Sub DirListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DirListBox1.SelectedIndexChanged
        FileListBox1.Path = DirListBox1.Path   ' When directory changes, set file path.
    End Sub

    Private Sub Drive2ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Drive2ListBox1.SelectedIndexChanged
        Dir2ListBox2.Path = Drive2ListBox1.Drive
    End Sub

    Private Sub Dir2ListBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Dir2ListBox2.SelectedIndexChanged
        File2ListBox3.Path = Dir2ListBox2.Path
    End Sub

    Private Sub FileListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FileListBox1.SelectedIndexChanged

    End Sub
    Private Sub FileListBox1_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles FileListBox1.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.All
        End If
    End Sub
    Private Sub FileListBox1_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles FileListBox1.DragDrop
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            Dim MyFiles() As String
            Dim i As Integer

            ' Assign the files to an array.
            MyFiles = e.Data.GetData(DataFormats.FileDrop)
            ' Loop through the array and add the files to the list.
            For i = 0 To MyFiles.Length - 1
                FileListBox1.Items.Add(MyFiles(i))
            Next
        End If
    End Sub
    Private Sub FileListBox1_MouseDown(ByVal sender As Object, ByVal e _
    As System.Windows.Forms.MouseEventArgs) Handles _
    FileListBox1.MouseDown, FileListBox1.MouseDown
        If FileListBox1.Items.Count = 0 Then
            Return
        End If
        Dim path As String = DirListBox1.Path
        Dim path1 As String = File2ListBox3.Path
        Dim index As Integer = FileListBox1.IndexFromPoint(e.X, e.Y)
        Dim sourceStr As String = FileListBox1.Items(index).ToString()
        Dim objDragDropEff As DragDropEffects = DoDragDrop(sourceStr, DragDropEffects.All)
        If objDragDropEff = DragDropEffects.All Then
            FileListBox1.Items.RemoveAt(FileListBox1.IndexFromPoint(e.X, e.Y))
        End If
        My.Computer.FileSystem.CopyFile(
    "" + path + "\" + sourceStr + "",
    "" + path1 + "\" + sourceStr + "",
    Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
    Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)
    End Sub
    Private Sub File2ListBox3_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles File2ListBox3.DragEnter
        e.Effect = DragDropEffects.All
    End Sub
    Private Sub File2ListBox3_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles File2ListBox3.DragDrop
        Dim str As String = CStr(e.Data.GetData(DataFormats.StringFormat))
        File2ListBox3.Items.Add(str)
    End Sub
End Class
