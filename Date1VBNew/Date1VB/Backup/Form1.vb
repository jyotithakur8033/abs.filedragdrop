Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtip As System.Windows.Forms.TextBox
    Friend WithEvents txtop As System.Windows.Forms.TextBox
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents DriveListBox1 As Microsoft.VisualBasic.Compatibility.VB6.DriveListBox
    Friend WithEvents DirListBox1 As Microsoft.VisualBasic.Compatibility.VB6.DirListBox
    Friend WithEvents FileListBox1 As Microsoft.VisualBasic.Compatibility.VB6.FileListBox
    Friend WithEvents lbDir As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtip = New System.Windows.Forms.TextBox
        Me.txtop = New System.Windows.Forms.TextBox
        Me.btn1 = New System.Windows.Forms.Button
        Me.btn2 = New System.Windows.Forms.Button
        Me.DriveListBox1 = New Microsoft.VisualBasic.Compatibility.VB6.DriveListBox
        Me.DirListBox1 = New Microsoft.VisualBasic.Compatibility.VB6.DirListBox
        Me.FileListBox1 = New Microsoft.VisualBasic.Compatibility.VB6.FileListBox
        Me.lbDir = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtip
        '
        Me.txtip.Location = New System.Drawing.Point(16, 24)
        Me.txtip.Name = "txtip"
        Me.txtip.TabIndex = 0
        Me.txtip.Text = ""
        '
        'txtop
        '
        Me.txtop.Location = New System.Drawing.Point(328, 24)
        Me.txtop.Name = "txtop"
        Me.txtop.TabIndex = 1
        Me.txtop.Text = ""
        '
        'btn1
        '
        Me.btn1.Location = New System.Drawing.Point(136, 24)
        Me.btn1.Name = "btn1"
        Me.btn1.TabIndex = 2
        Me.btn1.Text = "Format1"
        '
        'btn2
        '
        Me.btn2.Location = New System.Drawing.Point(232, 24)
        Me.btn2.Name = "btn2"
        Me.btn2.TabIndex = 3
        Me.btn2.Text = "Format2"
        '
        'DriveListBox1
        '
        Me.DriveListBox1.Location = New System.Drawing.Point(128, 80)
        Me.DriveListBox1.Name = "DriveListBox1"
        Me.DriveListBox1.Size = New System.Drawing.Size(152, 21)
        Me.DriveListBox1.TabIndex = 4
        '
        'DirListBox1
        '
        Me.DirListBox1.IntegralHeight = False
        Me.DirListBox1.Location = New System.Drawing.Point(16, 160)
        Me.DirListBox1.Name = "DirListBox1"
        Me.DirListBox1.Size = New System.Drawing.Size(168, 160)
        Me.DirListBox1.TabIndex = 5
        '
        'FileListBox1
        '
        Me.FileListBox1.Location = New System.Drawing.Point(256, 160)
        Me.FileListBox1.Name = "FileListBox1"
        Me.FileListBox1.Pattern = "*.*"
        Me.FileListBox1.Size = New System.Drawing.Size(184, 160)
        Me.FileListBox1.TabIndex = 6
        '
        'lbDir
        '
        Me.lbDir.Location = New System.Drawing.Point(16, 128)
        Me.lbDir.Name = "lbDir"
        Me.lbDir.Size = New System.Drawing.Size(168, 23)
        Me.lbDir.TabIndex = 7
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(200, 296)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 24)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "'Q'"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(456, 341)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lbDir)
        Me.Controls.Add(Me.FileListBox1)
        Me.Controls.Add(Me.DirListBox1)
        Me.Controls.Add(Me.DriveListBox1)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.txtop)
        Me.Controls.Add(Me.txtip)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn1.Click
        txtop.Text = Convert.ToDateTime(txtip.Text).ToString("MM/d/yy")
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        txtop.Text = Convert.ToDateTime(txtip.Text).ToString("d/MM/yy")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dispose()
    End Sub

    Private Sub DriveListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DriveListBox1.SelectedIndexChanged
        DirListBox1.Path = DriveListBox1.Drive ' When drive changes, set directory path.
    End Sub

    Private Sub DirListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DirListBox1.SelectedIndexChanged
        FileListBox1.Path = DirListBox1.Path   ' When directory changes, set file path.
    End Sub
End Class
